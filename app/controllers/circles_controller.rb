class CirclesController < ApplicationController
	def index
	end
	def show
		@circle_id=params[:id]
		@circle=Circle.find(@circle_id)
	end
	def new
		@circles=Circle.all
		@new_circle=current_user.circles.new
	end
	def create
		# binding.pry
		@new_circle=current_user.circles.create(:name => params[:circle][:name])
		redirect_to :action => 'new'
	end
	def destroy
		Circle.find(params[:id]).delete
		redirect_to :action => 'new'
	end
	def add_circle
		@friendship=Friendship.find(params[:id])
		@circles=current_user.circles.all
	end
	def create_circle
		@friendship=Friendship.find(params[:friendship])
		@circle=Circle.find(params[:circle])
		@friendship.circles << @circle
		redirect_to :action => 'new'
	end
end
