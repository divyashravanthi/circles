class FriendshipsController < ApplicationController
	def create
    @friendship1 = current_user.friendships.build(:friend_id => params[:friend_id],:status => "requested")
    @friendship2= User.find(params[:friend_id]).friendships.build(:friend_id => current_user.id, :status => "pending")
    if @friendship1.save && @friendship2.save
      flash[:notice] = "Added friend."
      redirect_to root_url
    else
      flash[:error] = "Unable to add friend."
      redirect_to root_url
    end
  end
  
  def destroy
    @friendship1 = Friendship.find(params[:id])
    @friendship2 = Friendship.where(:user_id => @friendship1.friend_id, :friend_id => @friendship1.user_id).first
    @friendship1.destroy
    @friendship2.destroy
    flash[:notice] = "Removed friendship."
    redirect_to current_user
  end
  def accept_friend
    @accept1=Friendship.find(params[:friendship])
    @accept1.status="accepted"
    @accept2=Friendship.where(:user_id => @accept1.friend_id, :friend_id => @accept1.user_id).first
    @accept2.status="accepted"
    @accept1.save && @accept2.save
    redirect_to root_url
  end
end
