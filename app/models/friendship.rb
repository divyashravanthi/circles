class Friendship < ActiveRecord::Base
	belongs_to :user
  belongs_to :friend, :class_name => "User"
  has_and_belongs_to_many :circles
end
