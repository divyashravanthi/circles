class User < ActiveRecord::Base
	has_many :friendships, :conditions => {:status => "accepted"}
	has_many :pending_friendships, :class_name => "Friendship", :conditions => {:status => "pending"}
	has_many :requested_friendships, :class_name => "Friendship", :conditions => {:status => "requested"}
	has_many :accepted_friendships, :class_name => "Friendship", :conditions => {:status => "accepted"}
	has_many :circles
  has_many :friends, :through => :accepted_friendships
  # has_many :requested_friends, :through => :requested_friendships
  # has_many :pending_friends, :through => :pending_friendships
  has_many :inverse_friendships, :class_name => "Friendship", :foreign_key => "friend_id"
  has_many :inverse_friends, :through => :inverse_friendships, :source => :user
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

end
