class CreateJoinTable < ActiveRecord::Migration
  def change
    create_join_table :circles, :friendships do |t|
      t.index [:circle_id, :friendship_id]
      t.index [:friendship_id, :circle_id]
    end
  end
end
